# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the ltris2 package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ltris2 2.0.3\n"
"Report-Msgid-Bugs-To: kulkanie@gmx.net\n"
"POT-Creation-Date: 2024-11-24 17:46+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/menu.h:167
msgid "Back"
msgstr ""

#: src/menu.h:283
msgid "Off"
msgstr ""

#: src/menu.h:283
msgid "On"
msgstr ""

#: src/menu.h:332
msgid "Enter Name"
msgstr ""

#: src/view.cpp:46 src/view.cpp:401
msgid "Normal"
msgstr ""

#: src/view.cpp:47
msgid "Figures"
msgstr ""

#: src/view.cpp:48
msgid "Vs Human"
msgstr ""

#: src/view.cpp:49
msgid "Vs CPU"
msgstr ""

#: src/view.cpp:50
msgid "Vs 2xCPU"
msgstr ""

#: src/view.cpp:74 src/vgame.cpp:214
msgid "(Re)"
msgstr ""

#: src/view.cpp:398
msgid "50 FPS"
msgstr ""

#: src/view.cpp:398
msgid "60 FPS"
msgstr ""

#: src/view.cpp:398
msgid "200 FPS"
msgstr ""

#: src/view.cpp:401
msgid "Defensive"
msgstr ""

#: src/view.cpp:401
msgid "Aggressive"
msgstr ""

#: src/view.cpp:402
msgid "Fullscreen"
msgstr ""

#: src/view.cpp:403
msgid "Small Window"
msgstr ""

#: src/view.cpp:403
msgid "Medium Window"
msgstr ""

#: src/view.cpp:403
msgid "Large Window"
msgstr ""

#: src/view.cpp:407
msgid ""
"This is your starting level which will be ignored for mode 'Figures' (always "
"starts at level 0).\n"
"\n"
"If not 0 the first level transition will require more lines to be cleared "
"(the higher the starting level the more lines)."
msgstr ""

#: src/view.cpp:411
msgid ""
"Normal: Starts with an empty bowl. Try to survive as long as possible.\n"
"\n"
"Figures: Clear a different figure each level. Later on single blocks (level "
"7+) or lines (level 13+) will appear.\n"
"\n"
"Vs Human/CPU/2xCPU: Play against another human or one or two CPU opponents."
msgstr ""

#: src/view.cpp:414
msgid ""
"Modern: Enables all that stuff that makes tetris casual like ghost piece, "
"wall-kicks, hold, DAS charge during ARE (allows to shift next piece faster), "
"piece bag (at max 12 pieces before getting same piece again).\n"
"\n"
"Classic: Doesn't give you any of this, making the game really hard but also "
"very interesting."
msgstr ""

#: src/view.cpp:432 src/view.cpp:545
msgid "New Game"
msgstr ""

#: src/view.cpp:433 src/view.cpp:455
msgid "Multiplayer Options"
msgstr ""

#: src/view.cpp:434 src/view.cpp:547
msgid "Controls"
msgstr ""

#: src/view.cpp:435 src/view.cpp:444 src/view.cpp:480
msgid "Player 1"
msgstr ""

#: src/view.cpp:436 src/view.cpp:445 src/view.cpp:481
msgid "Player 2"
msgstr ""

#: src/view.cpp:437 src/view.cpp:549
msgid "Audio"
msgstr ""

#: src/view.cpp:438 src/view.cpp:548
msgid "Graphics"
msgstr ""

#: src/view.cpp:442
msgid "Start Game"
msgstr ""

#: src/view.cpp:448
msgid "Game Mode"
msgstr ""

#: src/view.cpp:450
msgid "Game Style"
msgstr ""

#: src/view.cpp:451
msgid "Classic"
msgstr ""

#: src/view.cpp:451
msgid "Modern"
msgstr ""

#: src/view.cpp:452
msgid "Starting Level"
msgstr ""

#: src/view.cpp:460
msgid "Holes"
msgstr ""

#: src/view.cpp:461
msgid "Number of holes in sent lines."
msgstr ""

#: src/view.cpp:463
msgid "Random Holes"
msgstr ""

#: src/view.cpp:464
msgid "If multiple lines are sent, each line has different holes."
msgstr ""

#: src/view.cpp:467
msgid "CPU Style"
msgstr ""

#: src/view.cpp:468
msgid ""
"Defensive: Clears any lines.\n"
"Normal: Mostly goes for two lines.\n"
"Aggressive: Tries to complete three or four lines (on modern only).\n"
"\n"
"In general: The more aggressive the style, the more priority is put on "
"completing multiple lines at the expense of a balanced bowl contents."
msgstr ""

#: src/view.cpp:470
msgid "CPU Drop Delay"
msgstr ""

#: src/view.cpp:473
msgid "CPU Speed"
msgstr ""

#: src/view.cpp:483
msgid "Auto-Shift Delay"
msgstr ""

#: src/view.cpp:484
msgid "Initial delay before auto shift starts. Classic DAS has 270."
msgstr ""

#: src/view.cpp:486
msgid "Auto-Shift Speed"
msgstr ""

#: src/view.cpp:487
msgid "Delay between auto shift steps. Classic DAS has 100."
msgstr ""

#: src/view.cpp:494
msgid ""
"Left/Right: horizontal movement\n"
"Rotate Left/Right: piece rotation\n"
"Down: fast soft drop\n"
"Drop: INSTANT hard drop\n"
"Hold: put current piece to hold (only for modern style)"
msgstr ""

#: src/view.cpp:499
msgid "Left"
msgstr ""

#: src/view.cpp:500
msgid "Right"
msgstr ""

#: src/view.cpp:501
msgid "Rotate Left"
msgstr ""

#: src/view.cpp:502
msgid "Rotate Right"
msgstr ""

#: src/view.cpp:503
msgid "Down"
msgstr ""

#: src/view.cpp:504
msgid "Drop"
msgstr ""

#: src/view.cpp:505 src/vgame.cpp:481
msgid "Hold"
msgstr ""

#: src/view.cpp:511
msgid "Theme"
msgstr ""

#: src/view.cpp:512
msgid "Select theme. (not applied yet)"
msgstr ""

#: src/view.cpp:514
msgid "Mode"
msgstr ""

#: src/view.cpp:515
msgid "Select mode. (not applied yet)"
msgstr ""

#: src/view.cpp:517
msgid "Apply Theme&Mode"
msgstr ""

#: src/view.cpp:518
msgid "Apply the above settings."
msgstr ""

#: src/view.cpp:520
msgid "Animations"
msgstr ""

#: src/view.cpp:522
msgid "Smooth Drop"
msgstr ""

#: src/view.cpp:523
msgid "Drop piece block-by-block or smoothly. Does not affect drop speed."
msgstr ""

#: src/view.cpp:525
msgid "Frame Limit"
msgstr ""

#: src/view.cpp:531
msgid "Sound"
msgstr ""

#: src/view.cpp:532
msgid "Volume"
msgstr ""

#: src/view.cpp:534
msgid "Buffer Size"
msgstr ""

#: src/view.cpp:535
msgid ""
"Reduce buffer size if you experience sound delays. Might have more CPU "
"impact though. (not applied yet)"
msgstr ""

#: src/view.cpp:537
msgid "Channels"
msgstr ""

#: src/view.cpp:538
msgid ""
"More channels gives more sound variety, less channels less (not applied yet)"
msgstr ""

#: src/view.cpp:540
msgid "Apply Size&Channels"
msgstr ""

#: src/view.cpp:541
msgid "Apply above settings"
msgstr ""

#: src/view.cpp:551
msgid "Quit"
msgstr ""

#: src/view.cpp:894
msgid "Hiscores"
msgstr ""

#: src/vgame.cpp:478
msgid "Next"
msgstr ""

#: src/vbowl.cpp:101 src/vbowl.cpp:113
msgid "Lines"
msgstr ""

#: src/vbowl.cpp:104 src/vbowl.cpp:109
msgid "Level"
msgstr ""

#: src/vbowl.cpp:122
msgid "Paused"
msgstr ""

#: src/vbowl.cpp:124
msgid "Winner!"
msgstr ""

#: src/vbowl.cpp:126
msgid "Game Over"
msgstr ""

#: src/vbowl.cpp:253
msgid "Pieces Placed"
msgstr ""

#: src/vbowl.cpp:254
msgid "I-Pieces"
msgstr ""

#: src/vbowl.cpp:257
msgid "Singles"
msgstr ""

#: src/vbowl.cpp:258
msgid "Doubles"
msgstr ""

#: src/vbowl.cpp:259
msgid "Triples"
msgstr ""

#: src/vbowl.cpp:260
msgid "Tetrises"
msgstr ""

#: src/vbowl.cpp:270
msgid "Tetris Rate"
msgstr ""

#: src/vbowl.cpp:274
msgid "Droughts"
msgstr ""

#: src/vbowl.cpp:275
msgid "Drought Max"
msgstr ""

#: src/vbowl.cpp:279
msgid "Drought Avg"
msgstr ""

#: src/vbowl.cpp:283
msgid "Current Drought"
msgstr ""
